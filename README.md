Front end runs on port 5004
Back end runs on port 5005

Clone and pull

Front End
- Run "npm run start" to run dev
- Build production app by running "npm run build"
- Run "npm run prod" to run PM2 instance of app 

Back End
- Run "npm run start" to run dev
- Run "npm run forever" to run production