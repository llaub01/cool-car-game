const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

require('dotenv').config();

const app = express();
const port = process.env.PORT;

app.use(cors());
app.use(express.json());

const uri = process.env.ATLAS_URI;
mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true }
);
const connection = mongoose.connection;
connection.once('open', () => {
  console.log("MongoDB database connection established successfully");
}).catch((err) => {
  console.error(err.message); //Handles initial connection errors
  process.exit(1); // Exit process with failure
});

connection.on('error', () => {
  console.log('> error occurred from the database');
});

const carsRouter = require('./routes/cars');
const usersRouter = require('./routes/users');

app.use('/cars', carsRouter);
app.use('/users', usersRouter);

app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});
