const router = require('express').Router();
let Car = require('../models/car.model');

router.route('/').get((req, res) => {
  Car.find()
    .then(cars => res.json(cars))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
  const make = req.body.make;
  const model = req.body.model;
  const description = Number(req.body.description);
  const points = req.body.points;
  const imgurl = req.body.imgurl;

  const newCar = new Car({
    make,
    model,
    description,
    points,
    imgurl,
  });

  newCar.save()
  .then(() => res.json('Car added!'))
  .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').get((req, res) => {
  Car.findById(req.params.id)
    .then(car => res.json(car))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').delete((req, res) => {
  Car.findByIdAndDelete(req.params.id)
    .then(() => res.json('Car deleted.'))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/update/:id').put((req, res) => {
  Car.findById(req.params.id)
    .then(car => {
      car.make = req.body.make;
      car.model = req.body.model;
      car.description = req.body.description;
      car.points = req.body.points;
      car.imgurl = req.body.imgurl;

      car.save()
        .then(() => res.json('Car updated!'))
        .catch(err => res.status(400).json('Error: ' + err));
    })
    .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;