import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Navbar extends Component {

  render() {
    return (
      <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
        <Link to="/" className="navbar-brand">Cool Car Game</Link>
        <div className="collpase navbar-collapse">
        <ul className="navbar-nav mr-auto">
          <li className="navbar-item">
          <Link to="/cars" className="nav-link">Cars</Link>
          </li>
          <li className="navbar-item">
          <Link to="/car" className="nav-link">Create Car</Link>
          </li>
          <li className="navbar-item">
          <Link to="/user" className="nav-link">Create User</Link>
          </li>
          <li className="navbar-item">
          <Link to="/users" className="nav-link">Users</Link>
          </li>
          <li className="navbar-item">
          <Link to="/" className="nav-link">Game</Link>
          </li>
        </ul>
        </div>
      </nav>
    );
  }
}